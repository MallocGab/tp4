/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Gabriel
 */
public class FondExistantException extends Exception{
    FondExistantException(String msg){
        super(msg);
    }
    
    FondExistantException(){
        super("Fond existe déjà pour la clé fournie");
    }
}
