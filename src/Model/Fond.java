/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Gabriel
 */
public class Fond implements Comparable {
    private double amount;
    
    public Fond(){
        amount = 0;
    }
    
    public Fond(double _amount){
        amount = _amount;
    }
    
    //getters
    public double getAmount(){
        return amount;
    }
    
    //setters
    public void setAmount(double value){
        amount = value;
    }
    
    @Override
    public boolean equals(Object o){
        Fond  f = (Fond) o;
        return this.amount == f.amount;
    }

    @Override
    public int compareTo(Object o) {
        Fond  f = (Fond) o;
        if(this.amount < f.amount) return -1;
        else if(this.amount > f.amount) return 1;
        else if(this.amount == f.amount) return 0;
        else return 0;
    }
     
}
