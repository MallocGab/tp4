/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel
 */
public class Portefeuille implements Serializable {
    public HashMap<String, Fond> fondsHash;
    public HashMap<String, Instrument> intrumentsHash;

    public Portefeuille(){
        fondsHash = new HashMap<>();
        intrumentsHash = new HashMap<>();
    }
    
    public Portefeuille(HashMap fonds, HashMap instrument){
        fondsHash = fonds;
        intrumentsHash = instrument;
    }
    
    public double searchFond(String key) throws FondInexistantException{
        if(fondsHash != null && fondsHash.containsKey(key)) return fondsHash.get(key).getAmount();
        else throw new FondInexistantException();
    }
    
    public ArrayList searchInstrument(String key) throws InstrumentInexistantException{
        if (intrumentsHash != null && intrumentsHash.containsKey(key)) return intrumentsHash.get(key).getFonds();
        else throw new InstrumentInexistantException();
    }
    
    public void addFond(String key, int amount) throws FondExistantException{
        Fond f = new Fond(amount);
        if (fondsHash != null && fondsHash.containsKey(key)) throw new FondExistantException();
        else fondsHash.put(key,f);
    }
    
    public void addFondToInstrument(String keyInstrument, Fond f){
        //recherche s'il existe
        if (intrumentsHash != null && intrumentsHash.containsKey(keyInstrument)){
            //si oui alors aujoute le fond à cette instrument
            Instrument temp = intrumentsHash.get(keyInstrument);
            temp.addFondInstrument(f);
        }
        else {
            //si non alors créé un nouvel instrument et ajoute ce fond à l'instrument nouvellement créé
            Instrument inst = new Instrument();
            inst.addFondInstrument(f);
            intrumentsHash.put(keyInstrument,inst);
        }
    }
    
    public void deleteFond(String key){
        try {
            searchFond(key);
            if(fondsHash != null) fondsHash.remove(key);
        } catch (FondInexistantException ex) {
            Logger.getLogger(Portefeuille.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteInstrument(String key){
        try {
            searchInstrument(key);
            if(intrumentsHash != null){
                intrumentsHash.get(key).setFonds(null);
                intrumentsHash.remove(key);
            }
        } catch (InstrumentInexistantException ex) {
            Logger.getLogger(Portefeuille.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void serial(String filename){
        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            try{
                oos.writeObject(this);
                oos.flush();
                System.out.println("L'objet a été sérialisé");
            } finally {
                try {
                    oos.close();
                } finally {
                    fos.close();
                }
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Portefeuille.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Portefeuille.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deserial(String filename){
        Portefeuille temp = null;
        try {
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            try {
                try {
                    temp = (Portefeuille) ois.readObject();
                    
                    this.intrumentsHash = temp.intrumentsHash;
                    this.fondsHash = temp.fondsHash;
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Portefeuille.class.getName()).log(Level.SEVERE, null, ex);
                }
            } finally {
                try {
                    ois.close();
                } finally {
                    fis.close();
                }
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Portefeuille.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Portefeuille.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(temp != null){
            System.out.println("L'objet a été désérialisé");
        }
    }
            
}
