/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Gabriel
 */
public class Instrument {
    private ArrayList<Fond> fonds;

    public Instrument(){
        fonds = new ArrayList<>();
    }
    
    public Instrument(ArrayList<Fond> value){
        fonds = value;
    }
    
    public void addFondInstrument(Fond obj){
        fonds.add(obj);
    }
    
    //getters
    public ArrayList getFonds(){
        return fonds;
    }
    
    //setters
    public void setFonds(ArrayList value){
        fonds = value;
    }
    
    public void sort(){
        Collections.sort(fonds);
    }
}
