/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Gabriel
 */
public class InstrumentInexistantException extends Exception{
    InstrumentInexistantException(String msg){
        super(msg);
    }
    
    InstrumentInexistantException(){
        super("Instruement inexistant pour la clé fournie");
    }
}
