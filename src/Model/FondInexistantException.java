/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Gabriel
 */
public class FondInexistantException extends Exception {
    FondInexistantException(String msg){
        super(msg);
    }
    
    FondInexistantException(){
        super("Fond inexistant pour la clé fournie");
    }
}
