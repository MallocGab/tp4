/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp4;

import Model.Fond;
import Model.FondExistantException;
import Model.FondInexistantException;
import Model.Instrument;
import Model.InstrumentInexistantException;
import Model.Portefeuille;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel
 */
public class TP4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        /*
         * 1.5
         */
        
        System.out.println("Création d'un nouveau fond ... ");
        System.out.println("Nom du fond : ");
        String name = sc.nextLine();
        
        System.out.println("Valeur de ce fond :");
        int val = sc.nextInt();
        
        Portefeuille port = new Portefeuille();
        
        /*
        try {
            System.out.println("Recherche du fond ...");
            port.searchFond(name);
            System.out.println("Ce fond existe déjà ");
        } catch (FondInexistantException ex) {
            Logger.getLogger(TP4.class.getName()).log(Level.SEVERE, null, ex); 
            System.out.println("Fond inexistant" );
            try {
                System.out.println("Ajout du fond au portefeuille ...");
                port.addFond(name, val);
            } catch (FondExistantException e) {
                Logger.getLogger(TP4.class.getName()).log(Level.SEVERE, null, e);
            }
        } 
        
        sc.nextLine();
        /*
        * 1.6
        */
        /*
        System.out.println("Gestion d'un instrument ... ");
        System.out.println("Nom de l'instrument : ");
        name = sc.nextLine();
        String instru = name;
        try {
            System.out.println("Recherche de l'instrument ...");
            port.searchInstrument(name);
            System.out.println("Cet instrument existe");
        } catch (InstrumentInexistantException ex) {
            Logger.getLogger(TP4.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("instrument inexistant" );
            
            System.out.println("Création d'un nouvel instrument ...");
            Instrument inst = new Instrument();
            port.intrumentsHash.put(name,inst);
            
            System.out.println("Ajout du fond précédent à l'instrument ...");
            Fond f = new Fond(val);
            inst.getFonds().add(f);
        }
        
        /*
        * 1.7
        */
        /*
        System.out.println("Suppression d'un fond ...");
        System.out.println("Nom du fond : ");
        name = sc.nextLine();
        port.deleteFond(name);
        
        System.out.println("Suppression d'un instrument ...");
        System.out.println("Nom de l'instument : ");
        name = sc.nextLine();
        port.deleteInstrument(instru);
        
        sc.close();
        */
        
        Scanner sc2 = new Scanner(System.in);
        System.out.println("Entrez le nom du fichier dans lequel vous voulez sérialiser votre portefeuille : ");
        String file = sc2.nextLine();
        System.out.println("Serialisation ...");
        port.serial(file);
        
        
        System.out.println("Déserialisation ...");
        port.deserial(file);
    }
    
}
