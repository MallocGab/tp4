/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Fond;
import Model.FondInexistantException;
import Model.Instrument;
import Model.Portefeuille;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel
 */
public class OutputTerminal {
    public void printInstrument(HashMap<String, Instrument> inst){
        
        //https://beginnersbook.com/2013/12/how-to-sort-hashmap-in-java-by-keys-and-values/
        /*
        Iterator iterator = inst.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry me2 = (Map.Entry) iterator.next();
            System.out.println("Key: " + me2.getKey());
            for(Fond f : (ArrayList)me2.getValue()){
                
            }
        }
        */
       
        //https://stackoverflow.com/questions/4234985/how-to-for-each-the-hashmap
        for(Map.Entry<String, Instrument> entry : inst.entrySet()) {
            String key = entry.getKey();
            Instrument value = entry.getValue();
            ArrayList<Fond> al = value.getFonds();
            
            System.out.print("clé : " + key);
            System.out.print("\t nombre total de fonds : " + al.size());
            
            double tot= 0;
            for(Fond f : al){
                //System.out.println("\t value : " + f.getAmount());
                tot += f.getAmount();
            }
            System.out.print("\t somme totale des fonds : " + tot);
        }
    }
    
    public void printFond(Portefeuille p, String key){
        try {
            System.out.println("Valeur du fond : " + p.searchFond(key));
        } catch (FondInexistantException ex) {
            Logger.getLogger(OutputTerminal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
